export const SET_STEP = 'set_step';
export const SET_FORM_VALUE = 'set_form_value';
export const COMPLETED = 'completed'

export const STEP_ONE = 0;
export const STEP_TWO = 1;
export const STEP_THREE = 2;
export const STEP_FOUR = 3;