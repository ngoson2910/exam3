import { combineReducers } from "redux";
import completedReducer from "./completed";
import formReducer from "./form";
import stepReducer from "./step";


const rootReducer = combineReducers({
  step: stepReducer,
  form: formReducer,
  completed: completedReducer
})

export default rootReducer;