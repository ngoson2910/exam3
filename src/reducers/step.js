import {SET_STEP} from '../constants'

const initialState = {
  currentStep: 0,
}

function stepReducer(state = initialState, action) {
  switch(action.type) {
    case SET_STEP:
      return {
        ...state,
        currentStep: action.payload
      }
    default:
      return state;
  }
}

export default stepReducer;