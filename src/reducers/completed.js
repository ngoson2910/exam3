import {COMPLETED} from '../constants'

const initialState = {
  completed: false,
}

function completedReducer(state = initialState, action) {
  switch(action.type) {
    case COMPLETED:
      return {
        ...state,
        completed: action.payload
      }
    default:
      return state;
  }
}

export default completedReducer;