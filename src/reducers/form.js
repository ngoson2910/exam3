import {SET_FORM_VALUE} from '../constants'

const initialState = {
  firstName: '',
  lastName: '',
  email: '',
  confirmEmail: '', 
  password: '',
  confirmPassword: '',
  accept: false
}

function formReducer(state = initialState, action) {
  switch(action.type) {
    case SET_FORM_VALUE:
      return {
        ...state,
        ...action.payload
      }
    default:
      return state;
  }
}

export default formReducer;