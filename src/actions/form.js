import {SET_FORM_VALUE} from '../constants'

export const setFormValue = payload => ({
  type: SET_FORM_VALUE,
  payload
})