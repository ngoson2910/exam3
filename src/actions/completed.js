import {COMPLETED} from '../constants'

export const completed = payload => ({
  type: COMPLETED,
  payload
});
