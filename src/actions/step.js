import {SET_STEP} from '../constants'

export const setStep = payload => ({
  type: SET_STEP,
  payload
});
