import { useSelector } from "react-redux";

import StepHeader from "./components/StepHeader";
import StepOne from "./components/StepOne";
import StepTwo from "./components/StepTwo";
import StepThree from "./components/StepThree";
import StepFour from "./components/StepFour";
import Completed from "./components/Completed";

function App() {
  const currentStep = useSelector((state) => state.step.currentStep);
  const completed = useSelector((state) => state.completed.completed);

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-8 offset-md-2">
          {completed ? (
            <Completed />
          ) : (
            <>
              <StepHeader />
              <div className="row">
                <div className="col-md-6 offset-md-3">
                  {currentStep === 0 && <StepOne />}
                  {currentStep === 1 && <StepTwo />}
                  {currentStep === 2 && <StepThree />}
                  {currentStep === 3 && <StepFour />}
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
