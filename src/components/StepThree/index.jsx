import { FastField, Form, Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";

import * as Yup from "yup";
import PropTypes from 'prop-types'
import InputField from "../../customFields/InputField";
import "./StepThree.css";
import { STEP_FOUR, STEP_TWO } from "../../constants";
import { setFormValue } from "../../actions/form";
import { setStep } from "../../actions/step";

StepThree.propTypes = {
  onSubmit: PropTypes.func
}

StepThree.defaultProps = {
  onSubmit: null
}

function StepThree() {
  const { password, confirmPassword } = useSelector(state => state.form)
  const dispatch = useDispatch()
  const initialValues = {
    password: password,
    confirmPassword: confirmPassword,
  };

  const validationSchema = Yup.object().shape({
    password: Yup.string().required('This field is required!'),
    confirmPassword: Yup.string()
                  .oneOf([Yup.ref('password')], 'Password must match')
  })

  const handlePreviusStep = () => {
    dispatch(setStep(STEP_TWO))
  }

  return (
    <Formik 
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={values => {
        dispatch(setFormValue({
          password: values.password,
          confirmPassword: values.confirmPassword
        }))
        dispatch(setStep(STEP_FOUR))
      }}
    >
      {(formikProps) => {
        return (
          <Form>
            <FastField
              name="password"
              component={InputField}
              type="password"
              label="Password"
              placeholder="Enter your password"
            />

            <FastField
              name="confirmPassword"
              component={InputField}
              type="password"
              label="Confirm password"
              placeholder="Confirm password"
            />
            <div className="form-group d-flex justify-content-end">
              <button type="button" className="btn btn-info me-3" onClick={handlePreviusStep}>
                Previous
              </button>
              <button type="submit" className="btn btn-primary">
                Next
              </button>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}

export default StepThree;