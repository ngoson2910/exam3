import { FastField, Form, Formik } from 'formik';
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup'
import { completed } from '../../actions/completed';
import { setFormValue } from '../../actions/form';
import { setStep } from '../../actions/step';
import { STEP_THREE } from '../../constants';
import CheckboxField from '../../customFields/CheckboxField';
import './StepFour.css'

StepFour.propTypes = {
  onSubmit: PropTypes.func
}

StepFour.defaultProps = {
  onSubmit: null
}

function StepFour() {
  const {accept} = useSelector(state => state.form)
  const dispatch = useDispatch()
  const initialValues = {
    check: accept
  }
  const validationSchema = Yup.object().shape({
    check: Yup.boolean()
  })
  const handlePreviusStep = () => {
    dispatch(setStep(STEP_THREE))
  }
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={values => {
        dispatch(setFormValue({
          accept: values.check
        }))
        dispatch(completed(true))
      }}
    >
      {(formikProps) => {
        const {values} = formikProps
        return (
          <Form>
            <div className="ten columns terms">
              <span>By clicking "Accept" I agree that:</span>
              <ul className="docs-terms">
                <li>
                  I have read and accepted the<a href="#">User Agreement</a>
                </li>
                <li>
                  I have read and accepted the<a href="#">Privacy Policy</a>
                </li>
                <li>I am at least 18 years old</li>
              </ul>
              <FastField 
                name="check"
                checked={values.check}
                component={CheckboxField}
                label="Accept"
              />
            </div>
            <div className="form-group d-flex justify-content-end">
              <button type="button" className="btn btn-info me-3" onClick={handlePreviusStep}>
                Previous
              </button>
              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            </div>
          </Form>
        );
      }}
    </Formik>
  )
}

export default StepFour;