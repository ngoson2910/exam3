import { FastField, Form, Formik } from "formik";
import * as Yup from "yup";
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from "react-redux";
import InputField from "../../customFields/InputField";
import "./StepTwo.css";
import {setStep} from '../../actions/step'
import {setFormValue} from '../../actions/form'
import { STEP_ONE, STEP_THREE } from "../../constants";


StepTwo.propTypes = {
  onSubmit: PropTypes.func
}

StepTwo.defaultProps = {
  onSubmit: null
}

function StepTwo() {
  const {email, confirmEmail} = useSelector(state => state.form)
  const dispatch = useDispatch()
  const initialValues = {
    email: email,
    confirmEmail: confirmEmail,
  };

  const validationSchema = Yup.object().shape({
    email: Yup.string().email().required('This field is required!'),
    confirmEmail: Yup.string().email()
                  .oneOf([Yup.ref('email')], 'Email must match')
  })

  const handlePreviusStep = () => {
    dispatch(setStep(STEP_ONE))
  }
  return (
    <Formik 
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={values => {
        dispatch(setFormValue({
          email: values.email,
          confirmEmail: values.confirmEmail
        }))
        dispatch(setStep(STEP_THREE))
      }}
    >
      {(formikProps) => {
        return (
          <Form>
            <FastField
              name="email"
              component={InputField}
              label="Your email"
              placeholder="test@mailbox.com"
            />

            <FastField
              name="confirmEmail"
              component={InputField}
              label="Confirm email"
              placeholder="Confirm email"
            />
            <div className="form-group d-flex justify-content-end">
              <button type="button" className="btn btn-info me-3" onClick={handlePreviusStep}>
                Previous
              </button>
              <button type="submit" className="btn btn-primary">
                Next
              </button>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}

export default StepTwo;
