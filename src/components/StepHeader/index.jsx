import { useSelector } from "react-redux";
import { STEP_ONE, STEP_TWO, STEP_THREE, STEP_FOUR } from "../../constants";
import "./StepHeader.css";

function StepHeader() {
  const currentStep = useSelector((state) => state.step.currentStep);
  return (
    <ol className="progtrckr">
      <li
        className={`${currentStep === STEP_ONE ? "progtrckr-doing" : ""} ${
          currentStep > STEP_ONE ? "progtrckr-done" : ""
        }`}
      >
        <em>1</em>
        <span>StepOne</span>
      </li>
      <li
        className={`${currentStep === STEP_TWO ? "progtrckr-doing" : ""} ${
          currentStep > STEP_TWO ? "progtrckr-done" : ""
        } ${currentStep < STEP_TWO ? "progtrckr-todo" : ""}`}
      >
        <em>2</em>
        <span>StepTwo</span>
      </li>
      <li
        className={`${currentStep === STEP_THREE ? "progtrckr-doing" : ""} ${
          currentStep > STEP_THREE ? "progtrckr-done" : ""
        } ${currentStep < STEP_THREE ? "progtrckr-todo" : ""}`}
      >
        <em>3</em>
        <span>StepThree</span>
      </li>
      <li
        className={`${currentStep === STEP_FOUR ? "progtrckr-doing" : ""} ${
          currentStep > STEP_FOUR ? "progtrckr-done" : ""
        } ${currentStep < STEP_FOUR ? "progtrckr-todo" : ""}`}
      >
        <em>4</em>
        <span>StepFour</span>
      </li>
    </ol>
  );
}

export default StepHeader;
