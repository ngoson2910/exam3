import { useSelector } from 'react-redux'
import './Completed.css'

function Completed() {
  const { firstName, lastName, email} = useSelector(state => state.form)
  return(
    <>
      <h1>Completed</h1>
      <p>First Name: {firstName}</p>
      <p>Last Name: {lastName}</p>
      <p>Email: {email}</p>
    </>
  )
}

export default Completed