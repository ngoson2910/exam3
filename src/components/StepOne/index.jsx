import { FastField, Form, Formik } from "formik";
import * as Yup from "yup";
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from "react-redux";

import InputField from "../../customFields/InputField";
import "./StepOne.css";
import {setFormValue} from '../../actions/form'
import {setStep} from '../../actions/step'
import {STEP_TWO} from '../../constants'

StepOne.propTypes = {
  onSubmit: PropTypes.func
}

StepOne.defaultProps = {
  onSubmit: null
}

function StepOne() {
  const {firstName, lastName} = useSelector(state => state.form)
  const dispatch = useDispatch()
  const initialValues = {
    firstName: firstName,
    lastName: lastName,
  };

  const validationSchema = Yup.object().shape({
    firstName: Yup.string().required('This field is required!'),
    lastName: Yup.string().required('This field is required!')
  })
  return (
    <Formik 
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={values => {
        dispatch(setFormValue({
          firstName: values.firstName,
          lastName: values.lastName
        }))

        dispatch(setStep(STEP_TWO))
      }}
    >
      {(formikProps) => {
        return (
          <Form>
            <FastField
              name="firstName"
              component={InputField}
              label="First Name"
              placeholder="First Name"
            />

            <FastField
              name="lastName"
              component={InputField}
              label="Last Name"
              placeholder="Last Name"
            />
            <div className="form-group d-flex justify-content-end">
              <button type="submit" className="btn btn-primary">
                Next
              </button>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}

export default StepOne;
